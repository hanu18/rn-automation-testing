describe('Pokemon data is loading', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  // it("should show Pokeball image on app load", async () => {
  //   await expect(element(by.id("pokeball_image"))).toBeVisible(); // 75% of the tested component should be visible on the screen
  // });


  it("should show relevant Pokemon data after clicking the button and loading the data from the API", async () => {
    await element(by.id("action_button")).tap(); // click the button

    // verify that the components showing the relevant data exists
    await expect(element(by.id("pokemon_sprite"))).toExist();
    await expect(element(by.id("pokemon_name"))).toExist();
    await expect(element(by.id("pokemon_types"))).toExist();
    await expect(element(by.id("pokemon_description"))).toExist();
  });

});
