# rn_auto_test

This project has automation testing support for RN Android and iOS project 

# Basic Detox setups for iOS and Android 
React Native testing tools

## Jest
Jest is the tool that we’ll be using to implement snapshot testing. The main idea behind snapshot testing is that it allows you to test whether a component has changed. 
1. Implement the components.
2. Write tests that will verify if your component is rendering correctly.
3. Run the tests. This will create a yourTestName.snap file for each of your test files inside the _tests_/_snapshots_ directory. These files contain what’s actually being rendered by the components you are testing. We’ll go through this in more detail on a later section.
4. Commit the changes to your version control system.
5. Every time you make a change, you run the tests again. The tests which already have .snap files will have their output compared to that existing snapshot file. If they don’t match, then there might be an unexpected change with your code. Once you’ve verified that the change is correct, you can either update the existing snapshot or revert the changes you made to your component. This way, the tests will pass again.
As you can see, snapshot testing isn’t test-driven development. Mainly because you have to write the component first before taking a snapshot. Snapshot testing is mainly used for checking for unexpected changes in your code, and not for directing how you design your code.

## Configuring Jest
When you create a new React Native project, it already comes with Jest pre-installed and configured. 
Note:
On your package.json file, update this 
"jest": {
      "preset": "react-native",
      "testMatch": [
        "<rootDir>/__tests__/*"
      ]
    },
This tells Jest to only look for test files inside the _tests_ directory.
————————————————————————————————————————————————————————

## Detox
https://www.pusher.com/tutorials/react-native-development-tools-part-3-testing-tools

Detox is used for automating the tests that real users would normally do. If you’re coming from a web development background, Detox’s counterpart is Selenium. The workflow looks something like this:
1. Write code that will automate user interactions. Things like clicking a button, or typing some text in a text field.
2. Write code that will verify that the desired effect of that action is met. For example, when a user clicks a button, you want a specific text to show on the screen.
3. Run the tests to verify the functionality.

### Setup Detox steps
1. Upgrade to Gradle 3

android/build.gradle

dependencies {
        // classpath 'com.android.tools.build:gradle:2.2.3' 
        classpath 'com.android.tools.build:gradle:3.1.0' // replace the above with this
      }

android/gradle/wrapper/gradle-wrapper.properties

distributionUrl=https\://services.gradle.org/distributions/gradle-4.4-all.zip

Once that’s done, execute react-native run-android to install the new Gradle and verify that the app is working correctly. 

If you only want to test on Android, you’ll only need to globally install the following:
* 		Node 8.3.0 - higher version of Node may also work, but I haven’t personally tested it.
* 		Detox CLI

nvm install 8.3.0
    nvm alias default 8.3.0
    nvm use default
node --version

After that, you’ll need to install the react-native-cli if you’re using some other Node version before you installed 8.3.0:
    npm install -g react-native-cli

Also install Yarn, we’ll be using it to install the local dependencies for the React Native project:
    npm install -g yarn

Lastly, install the Detox command line tool:
    npm install -g detox-cli

### iOS setup
In order to test in iOS, you still need to have a Mac. I assume you already have brew installed so you can simply execute the following commands to install the Apple simulator utilities. Detox uses it to communicate with the iOS simulator:
    brew tap wix/brew
    brew install applesimutils

### Setting up Detox in the project

    yarn add detox --dev or npm install detox --save-dev

    yarn add mocha --dev or npm install mocha --save-dev

### Add Detox config to package.json
```
"detox": {
  "configurations": {
    "ios.sim.debug": {
      "binaryPath": "ios/build/Build/Products/Debug-iphonesimulator/example.app",
      "build": "xcodebuild -project ios/example.xcodeproj -scheme example -configuration Debug -sdk iphonesimulator -derivedDataPath ios/build",
      "type": "ios.simulator",
      "name": "iPhone 7"
    }
  }
}
```

### Set up test-code scaffolds 
detox init -r mocha

detox init

Build your app and run Detox tests
detox build
detox test


## Android setup

### update android/settings.gradle
```
rootProject.name = 'rntdd'
    include ':app'
    # add the below config
    include ':detox'
    project(':detox').projectDir = new File(rootProject.projectDir, '../node_modules/detox/android/detox')
```

Next, under android, update the compileSdkVersion, buildToolsVersion to version 27. Then under android → defaultConfig, set minSdkVersion to 18 and targetSdkVersion to 26:

### update android/build.gradle
```
buildscript {
    repositories {
        jcenter()
        google()
    }
    ext.kotlinVersion = '1.3.0'
    dependencies {
        classpath 'com.android.tools.build:gradle:3.1.0'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        mavenLocal()
        jcenter()
        google()
        maven {
            // All of React Native (JS, Obj-C sources, Android binaries) is installed from npm
            url "$rootDir/../node_modules/react-native/android"
        }
    }
}
```

### update app/build.gradle
```
android {
    compileSdkVersion 27
    buildToolsVersion "27.0.2"

    defaultConfig {
        applicationId "com.rntesting"
        minSdkVersion 18
        targetSdkVersion 26
        versionCode 1
        versionName "1.0"
        ndk {
            abiFilters "armeabi-v7a", "x86"
        }
        testBuildType System.getProperty('testBuildType', 'debug')  // This will later be used to control the test apk build type
        testInstrumentationRunner 'androidx.test.runner.AndroidJUnitRunner'
    }
    splits {
        abi {
            reset()
            enable enableSeparateBuildPerCPUArchitecture
            universalApk false  // If true, also generate a universal APK
            include "armeabi-v7a", "x86"
        }
    }
    buildTypes {
        release {
            minifyEnabled enableProguardInReleaseBuilds
            proguardFiles getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro"
        }
    }
    // applicationVariants are e.g. debug, release
    applicationVariants.all { variant ->
        variant.outputs.each { output ->
            // For each separate APK per architecture, set a unique version code as described here:
            // http://tools.android.com/tech-docs/new-build-system/user-guide/apk-splits
            def versionCodes = ["armeabi-v7a":1, "x86":2]
            def abi = output.getFilter(OutputFile.ABI)
            if (abi != null) {  // null for the universal-debug, universal-release variants
                output.versionCodeOverride =
                        versionCodes.get(abi) * 1048576 + defaultConfig.versionCode
            }
        }
    }
}

dependencies {
    compile fileTree(dir: "libs", include: ["*.jar"])
    compile "com.android.support:appcompat-v7:23.0.1"
    compile "com.facebook.react:react-native:+"  // From node_modules

   // androidTestImplementation('com.wix:detox:+') { transitive = true }
    androidTestImplementation 'junit:junit:4.12'
    androidTestImplementation(project(path: ":detox"))
}

// Run this once to be able to run the application with BUCK
// puts all compile dependencies into folder libs for BUCK to use
task copyDownloadableDepsToLibs(type: Copy) {
    from configurations.compile
    into 'libs'
}
```

### update settings.gradle
```
rootProject.name = 'rntesting'
include ':app'
include ':detox'
project(':detox').projectDir = new File(rootProject.projectDir, '../node_modules/detox/android/detox')
```
### update Create Android Test class
Add the file android/app/src/androidTest/java/com/[your.package]/DetoxTest.java and fill as in the detox example app for NR. Don't forget to change the package name to your project's.
```
package com.rntesting;

import com.wix.detox.Detox;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DetoxTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false, false);

    @Test
    public void runDetoxTests() {
        Detox.runTests(mActivityRule);
    }
}
```

### update Add Android configuration
Add this part to your package.json:
```
"detox" : {
    "configurations": {
        "android.emu.debug": {
            "binaryPath": "android/app/build/outputs/apk/debug/app-debug.apk",
            "build":
            "cd android && ./gradlew assembleDebug assembleAndroidTest -DtestBuildType=debug && cd ..",
            "type": "android.emulator",
            "name": "Nexus_5X_API_24"
        },
        "android.emu.release": {
            "binaryPath": "android/app/build/outputs/apk/release/app-release.apk",
            "build":
            "cd android && ./gradlew assembleRelease assembleAndroidTest -DtestBuildType=release && cd ..",
            "type": "android.emulator",
            "name": "Nexus_5X_API_26"
        }
    }
}
```

android.emulator. Boot stock SDK emulator with provided name, for example Nexus_5X_API_25. After booting connect to it.

### Run the tests
Using the android.emu.debug configuration from above, you can invoke it in the standard way.
detox test -c android.emu.debug

detox build

react-native start

detox test -c android.emu.debug # for android

detox test -c  ios.sim.debug #for ios

detox test -c android.emu.debug --reuse # to reuse existing terminal 

### Need to export ANDROID_SDK_ROOT path
/Users/macmini21/Library/Android/sdk

source ~/.bash_profile


export PATH="$PATH:/Users/{{your user}}/Desktop/softwares/flutter/bin"

export ANDROID_HOME=/Users/{{your user}}/Library/Android/sdk

export ANDROID_SDK_ROOT=/Users/{{your user}}/Library/Android/sdk

export ANDROID_AVD_HOME=/Users/{{your user}}/.android/avd


Adding a test
react-native run-android
react-native run-ios

